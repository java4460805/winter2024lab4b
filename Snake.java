public class Snake{
	
	private boolean venomous;
	private String species;
	private double lengthInMeters;
	
	public Snake(double l, String s, boolean v){
		this.venomous = v;
		this.species = s;
		this.lengthInMeters = l;
	    
	}
	public void getBit(){
		System.out.println("OH NO! I got bit by a "+species+". Is it venomous?");
		if (venomous){
			System.out.println("It's over guys, it was nice meeting you...");
		}else{
			System.out.println("Thank god! We live to see another day.");
		}
	}
	public void eatsWhat(){
		System.out.println("My snake is a "+species+" and is "+lengthInMeters+" meters long. What should i feed it?");
		if (lengthInMeters >= 3.0){
			System.out.println("You gotta feed him large animals");
		}else if( lengthInMeters < 3.0 && lengthInMeters >= 1.0){
			System.out.println("You can feed him smaller animals");
		}else{
			System.out.println("You can feed it anything smaller than him");
		}
	}
	
	public void setVenom(boolean venomous){
		this.venomous = venomous;
	}
	
	public boolean getVenom(){
		return this.venomous;
	}
	
	
	public String getSpecies(){
		return this.species;
	}
	
	
	public double getLength(){
		return this.lengthInMeters;
	}


}